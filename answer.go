package dekaants

type Answer interface {
	// AddEdge adds edge to path
	AddEdge(edge Edge)
	// Edges returns edges in answer
	Edges() []Edge

	// Metric returns metric of the path
	Metric() float64

	// Completed returns true if answer is fully completed
	Completed() bool

	// SetEnvironment sets environment for this answer
	SetEnvironment(env Environment)

	// Reset remove all edges from answer and makes him incompleted
	Reset()

	// Error return not-nil error if solution broken
	Error() error

	// Answer
	Copy() Answer
}
