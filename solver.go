package dekaants

type Solver interface {
	Solve(Environment) Answer
	Stop()
}
