package dekaants

type MaxMinStrategyConfig struct {
	MinMark       float64
	MaxMark       float64
	EvaporateCoff float64
}

func MakeMaxMinStrategyConfig() MaxMinStrategyConfig {
	return MaxMinStrategyConfig{
		MinMark:       0.01,
		MaxMark:       1,
		EvaporateCoff: 0.2,
	}
}

type maxMinStrategy struct {
	cfg MaxMinStrategyConfig

	environment   Environment
	iterationBest Answer
}

func NewMaxMinStrategy(cfg MaxMinStrategyConfig) SolverStrategy {
	var out maxMinStrategy
	out.cfg = cfg
	out.environment = nil

	return &out
}

func (s *maxMinStrategy) SetEnvironment(env Environment) {
	s.environment = env
}

func (s *maxMinStrategy) InitEdges() {
	var edges = s.environment.Edges()
	for _, e := range edges {
		e.SetMark(MarkValue(s.cfg.MaxMark))
	}
}

func (s *maxMinStrategy) SetIteration(iterationNumber uint64) {
	// Don't care
}

func (s *maxMinStrategy) SetIterationBestAnswer(answer Answer) {
	s.iterationBest = answer
}

func (s *maxMinStrategy) SetGlobalBestAnswer(answer Answer) {
	// Don't care
}

func (s *maxMinStrategy) UpdateMark() {
	// Evaporate
	var edges = s.environment.Edges()
	for _, e := range edges {
		var m = e.Mark()
		m *= MarkValue(1 - s.cfg.EvaporateCoff)
		m = s.clamp(m, MarkValue(s.cfg.MinMark), MarkValue(s.cfg.MaxMark))
		e.SetMark(m)
	}
	// Marking
	var itBestEdges = s.iterationBest.Edges()
	for _, e := range itBestEdges {
		var m = e.Mark()
		m += MarkValue(1.0 / s.iterationBest.Metric())
		m = s.clamp(m, MarkValue(s.cfg.MinMark), MarkValue(s.cfg.MaxMark))
		e.SetMark(m)
	}

}

func (s *maxMinStrategy) clamp(val, min, max MarkValue) MarkValue {
	if val < min {
		return min
	} else if max < val {
		return max
	}
	return val
}
