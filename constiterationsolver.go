package dekaants

type constIterationSolver struct {
	maxIterations  uint64
	populationSize uint
	factory        Factory

	verboseLayer VerboseLayer

	stopRequested bool
}

func NewConstIterationSolver(iterationNumber uint64, populationSize uint, factory Factory, layer VerboseLayer) Solver {
	var out = &constIterationSolver{
		maxIterations:  iterationNumber,
		populationSize: populationSize,
		factory:        factory,
		verboseLayer:   layer,
		stopRequested:  false,
	}

	if layer == nil {
		out.verboseLayer = NewNullVerboseLayer()
	}

	return out
}

func (s *constIterationSolver) Solve(env Environment) Answer {
	var strategy = s.factory.CreateSolverStrategy()
	strategy.SetEnvironment(env)
	strategy.InitEdges()

	var bestAnswer Answer = nil
	var bestMetric float64

	var colony *colony = newColony(s.populationSize, env, s.factory).(*colony)
	colony.Reset()
	defer colony.close()

	// initial iteration

	strategy.SetIteration(0)
	s.verboseLayer.OnIterationEnter(0)

	s.iteration(colony)
	bestAnswer = colony.Answer().Copy()

	if bestAnswer.Error() != nil {
		return bestAnswer
	}

	bestMetric = bestAnswer.Metric()
	strategy.SetIterationBestAnswer(bestAnswer)
	strategy.SetGlobalBestAnswer(bestAnswer)
	s.verboseLayer.OnIterationSolutionReady(bestAnswer)
	s.verboseLayer.OnBestAnswerChanged(bestAnswer)
	s.verboseLayer.IterationMetrics(colony.minMetric, colony.avgMetric, colony.maxMetric)

	strategy.UpdateMark()
	colony.Reset()

	var iterationNumber uint64
	for iterationNumber = 1; iterationNumber < s.maxIterations; iterationNumber++ {
		if s.stopRequested {
			break
		}

		strategy.SetIteration(iterationNumber)
		s.verboseLayer.OnIterationEnter(iterationNumber)

		s.iteration(colony)
		var itAnswer = colony.Answer().Copy()

		if itAnswer.Error() != nil {
			return itAnswer
		}

		strategy.SetIterationBestAnswer(itAnswer)
		s.verboseLayer.OnIterationSolutionReady(itAnswer)
		s.verboseLayer.IterationMetrics(colony.minMetric, colony.avgMetric, colony.maxMetric)

		if itAnswer.Metric() < bestMetric {
			bestAnswer = itAnswer.Copy()
			bestMetric = bestAnswer.Metric()
			strategy.SetGlobalBestAnswer(bestAnswer)
			s.verboseLayer.OnBestAnswerChanged(bestAnswer)
		}

		strategy.UpdateMark()
		colony.Reset()
	}

	return bestAnswer
}

func (s constIterationSolver) iteration(colony ant) {
	for !colony.Ready() {
		colony.Step()
	}
}

func (s *constIterationSolver) Stop() {
	s.stopRequested = true
}
