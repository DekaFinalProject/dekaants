package dekaants

type VerboseLayer interface {
	OnIterationEnter(i uint64)
	OnIterationSolutionReady(ans Answer)
	OnBestAnswerChanged(ans Answer)

	IterationMetrics(min, avg, max float64)
}

type NullVerboseLayer struct{}

func NewNullVerboseLayer() VerboseLayer {
	return NullVerboseLayer{}
}

func (l NullVerboseLayer) OnIterationEnter(uint64) {

}

func (l NullVerboseLayer) OnIterationSolutionReady(Answer) {

}

func (l NullVerboseLayer) OnBestAnswerChanged(Answer) {

}

func (l NullVerboseLayer) IterationMetrics(min, avg, max float64) {

}
