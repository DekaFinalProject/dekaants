package dekaants

type EdgeChooseStrategy interface {
	// SetEnvironment sets environment for this strategy
	SetEnvironment(env Environment)

	// ChooseEdge chooses edge from graph
	ChooseEdge(answer Answer) Edge
}

type SolverStrategy interface {
	// SetEnvironment sets environment for this strategy
	SetEnvironment(env Environment)

	// InitEdges init edges marks
	InitEdges()

	// SetIteration sets current iteration
	SetIteration(iterationNumber uint64)
	// SetIterationBestAnswer sets best answer in current iteration
	SetIterationBestAnswer(answer Answer)
	// SetGlobalBestAnswer sets best answer in whole run
	SetGlobalBestAnswer(answer Answer)

	// UpdateMark updates mark in environment
	UpdateMark()
}
