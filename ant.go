package dekaants

type ant interface {
	Step()
	Answer() Answer
	Ready() bool

	Reset()
}

type simpleAnt struct {
	answer   Answer
	strategy EdgeChooseStrategy
}

func newAnt(ans Answer, s EdgeChooseStrategy) ant {
	return &simpleAnt{
		answer:   ans,
		strategy: s,
	}
}

func (a *simpleAnt) Step() {
	if a.Ready() {
		return
	}

	var edge = a.strategy.ChooseEdge(a.Answer())
	a.answer.AddEdge(edge)
}

func (a simpleAnt) Answer() Answer {
	return a.answer
}

func (a simpleAnt) Ready() bool {
	return a.answer.Completed()
}

func (a *simpleAnt) Reset() {
	a.answer.Reset()
}
