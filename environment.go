package dekaants

type MarkValue float32
type Identifier int64

type Node interface {
	// ID returns id of the node
	ID() Identifier
}

type Edge interface {
	// From returns start of the edge
	From() Node
	// To returns end of the edge
	To() Node

	// SetMark sets mark for ants
	SetMark(newValue MarkValue)
	// Mark returns mark
	Mark() MarkValue

	// Metric returns Additional metric for choosing path
	Metric() float64
}

type Environment interface {
	// NodesIDs returns slice of identifiers of nodes
	NodesIDs() []Identifier
	// Node returns
	Node(id Identifier) Node

	// Edges returns all edges in environment
	Edges() []Edge

	// From returns edges from node
	From(nodeID Identifier) []Edge

	// StartNode returns start node
	StartNode() Node
	// EndNode returns end node
	EndNode() Node
}
