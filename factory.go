package dekaants

type Factory interface {
	CreateAnswer() Answer
	CreateEdgeChooseStrategy() EdgeChooseStrategy
	CreateSolverStrategy() SolverStrategy
}
