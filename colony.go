package dekaants

import "runtime"

// colony is composite pattern for ants
type colony struct {
	ants []ant

	metricInitialized bool
	minMetric         float64
	avgMetric         float64
	maxMetric         float64

	completed bool

	antNmbToStepChan chan int
	antCompletedChan chan int
}

func newColony(population uint, env Environment, factory Factory) ant {
	var c colony
	c.ants = make([]ant, population)
	c.completed = false
	c.antNmbToStepChan = make(chan int, population)
	c.antCompletedChan = make(chan int, population)

	for i := range c.ants {
		var ans = factory.CreateAnswer()
		var strategy = factory.CreateEdgeChooseStrategy()
		ans.SetEnvironment(env)
		strategy.SetEnvironment(env)
		c.ants[i] = newAnt(ans, strategy)
	}

	// Start threads for ants
	for i := 0; i < runtime.NumCPU(); i++ {
		go c.processFunc()
	}

	return &c
}

func (c *colony) Step() {
	if c.Ready() {
		return
	}

	// Tell ants to build solution
	for antNmb := range c.ants {
		c.antNmbToStepChan <- antNmb
	}

	// Waiting for ants builds their solution
	for range c.ants {
		antNmb := <-c.antCompletedChan
		var a = c.ants[antNmb]

		if c.metricInitialized {
			if a.Answer().Metric() < c.minMetric {
				c.minMetric = a.Answer().Metric()
			}
			if c.maxMetric < a.Answer().Metric() {
				c.maxMetric = a.Answer().Metric()
			}
		} else {
			c.minMetric = a.Answer().Metric()
			c.maxMetric = a.Answer().Metric()
			c.metricInitialized = true
		}
		c.avgMetric += a.Answer().Metric()
	}

	c.avgMetric /= float64(len(c.ants))

	c.completed = true
}

func (c colony) Answer() Answer {
	// Returns best in colony
	var bestAnswer = c.ants[0].Answer()
	var bestMetric = bestAnswer.Metric()

	for _, ant := range c.ants {
		if ant.Answer().Metric() < bestMetric {
			bestAnswer = ant.Answer()
			bestMetric = bestAnswer.Metric()
		}
	}

	return bestAnswer
}

func (c colony) Ready() bool {
	return c.completed
}

func (c *colony) Reset() {
	c.metricInitialized = false
	c.maxMetric = 0
	c.minMetric = 0
	c.avgMetric = 0

	c.completed = false
	for _, a := range c.ants {
		a.Reset()
	}
}

func (c *colony) stepAnt(antNumber int) {
	for !c.ants[antNumber].Ready() {
		c.ants[antNumber].Step()
	}
}

func (c *colony) close() {
	close(c.antNmbToStepChan)
}

func (c *colony) processFunc() {
	for antNmb := range c.antNmbToStepChan {
		c.stepAnt(antNmb)
		c.antCompletedChan <- antNmb
	}
}
